# -*- coding: utf-8 -*-
"""
Created on Fri Sep  3 10:28:42 2021

@author: nicko
"""
#import the json file from ncdc on historical weather and climate events.
import urllib, json
from collections import Counter

url = "https://www.ncdc.noaa.gov/billions/events-US-1980-2021.json"
resonse = urllib.request.urlopen(url)
data = json.loads(resonse.read())
#print(data) #toreview the json file to see title and data dictionaries

#to identify the keys and values within the data dictionary
json_data = data['data']
for x in json_data:
    keys = x.keys()
    #print(keys)
    values = x.values()
    #print(values)
    
#print(type(keys)) result was class 'dict_keys'
#print(type(values)) result was class 'dict_values'

#test to determine where in json I am
#print(data['data'][0])
#print(json_data)
#print(json_data[1])

#json_data is a list of dictionaries and disaster is index 1 in the dictionary

#to return the type of disasters
new_key = 'disaster'
disasters = [a_dict[new_key] for a_dict in json_data]
#print(disasters)


#this provides the uniques types of disasters tracked in the json by creating a set then sorting the list
disasters_set = set(disasters)
disasters_list = list(disasters_set)
count_disasters = len(disasters_list)
disasters_list.sort()
print('This .json tracks the following {} disasters: {}'.format(count_disasters, disasters_list)

#this provides a count of events tracked in the list
count_a = len(disasters)
print('\nThere were disaster {} events that occured between 1980 and 2020.'.format(count_a))


#using Counter from collections to count how many times each disaster occured and stores it as a dictionary.
counter_dict = Counter(disasters)
print(counter_dict)

#pulling totalCosts of each storm from json_data and convert strings to floats for summing
cost_key = 'totalCost'
costs = [a_dict[cost_key] for a_dict in json_data]
#TBD appeared in list, cannot convert to float for summing
costs.remove('TBD')
#float the list of strings
costs_float = [float(price) for price in costs]
total = sum(costs_float)

#pulling deaths of each storm from json_data and convert strings to ints for summing.
death_key = 'deaths'
deaths = [a_dict[death_key] for a_dict in json_data]
#float the list of strings
deaths_int = [int(dead) for dead in deaths]
lives = sum(deaths_int)

#print output of costs dollars and deaths
print("\nThe total cost of these disaster events is currently at${} Billion dollars and {} lives.".format(total, lives))



        
        







                                    


    
